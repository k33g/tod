const express = require("express");
const bodyParser = require("body-parser");

// TODO: see how to load external script (from repository for example)
const actions = require(`../${process.env.BOT_BRAIN}` || '../actions')

const hooks = require(`../${process.env.BOT_HOOKS}` || '../hooks')

const Robot = require('../robot/Robot')
let words = require(`../robot/features/words.js`)
let text = require(`../robot/features/text.js`)
let similarity = require(`../robot/features/similarity.js`)
let hooksEvents = require(`../robot/features/hooksEvents.js`)
let hooksGitLabEvents = require(`../robot/features/hooksGitLabEvents.js`)
let hooksGitHubEvents = require(`../robot/features/hooksGitHubEvents.js`)

/* ----- ----- */
const Context = require('../contexts/Context')
const contextMiddleWare = require(`../contexts/middlewares/${process.env.BOT_MIDDLEWARE || 'console/console.js'}`)
const contextHooks = require(`../contexts/features/hooks.js`)
const contextFeatures = require(`../contexts/features/features.js`)

const port = process.env.PORT || 9090

const app = express();

let robot = new Robot({
    name:process.env.BOT_NAME || "tod",
    trigger: process.env.BOT_TRIGGER || "@tod"
  }
  , words
  , text
  , similarity
  , hooksEvents
  , hooksGitLabEvents
  , hooksGitHubEvents
) 



if(process.env.WELCOME_MESSAGE) {
  robot.send({
    message: process.env.WELCOME_MESSAGE, 
    context:  new Context({request:null, response:null}, contextMiddleWare)
  })
}


app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))

app.use(express.static('public'));

app.get('/hello', (req, res) => {
  res.send({message: "Hello World!"})
});

app.post('/hooks/tod', (req, res) => {

  let context = new Context({request:req, response:res}, contextMiddleWare, contextFeatures, contextHooks)

  hooks({
    robot:robot, 
    context: context,
  })

  res.send({message: "POST:OK"})
})

app.post('/hey/tod', (req, res) => {
  //console.log(req.body)
  // If I want several contexts (eg: Mattermost + Slack)
  //   -> Several bots Instances with the same actions
  // If I want several contexts (eg: Mattermost + GitLab)
  //   -> Create a SCMContext 
  // Something about deployment ?

  let context = new Context({request:req, response:res}, contextMiddleWare, contextFeatures)

  if (context.checkAuthenticationToken()) { // this is the token from the Chat `CHAT_TOKEN`

    actions({
      robot:robot, 
      context: context,
    })
    
    res.send({message: "POST:OK"})
  } else {
    //TODO: throw error
    res.send({message: "POST:KO"})
  }



});

app.listen(port);
console.log(`🦊 ${robot.name} the 🤖 is started (trigger me with ${robot.trigger}) - listening on ${port}`);