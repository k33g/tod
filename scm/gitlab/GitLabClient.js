const SourceControl = require('../SourceControl')

String.prototype.replaceAll = function(search, replacement) {
  var target = this;
  return target.split(search).join(replacement);
};

function gitLabConnector({baseUri, token}) {
  return {
    baseUri: baseUri,
    token: token,
    headers: {
      "Content-Type": "application/json",
      "Private-Token": token
    }
  }
}

class GitLabClient extends SourceControl {
  constructor({baseUri, token}) {
    super({
      kind: gitLabConnector({
        baseUri: baseUri, 
        token: token
      }) 
    })

  }

  // ----- Users -----

  fetchUserByHandle(handle) { // get user data
    return this.getData({path:`/users?username=${handle}`})
      .then(response => response[0])
      .catch(error => error);
  }

  fetchUserById(id) { // get user data
    return this.getData({path:`/users/${id}`})
      .then(response => response)
      .catch(error => error);
  }

  fetchUser({handle}) {
    return this.getData({path:`/users?username=${handle}`})
      .then(response => {
        return this.getData({path:`/users/${response[0].id}`})
        
      })
  }

  fetchProjectsOfUser({handle, perPage}) {
    return this.getData({path:`/users?username=${handle}`})
      .then(response => {
        return this.getData({path:`/users/${response[0].id}/projects?per_page=${perPage==undefined?20:perPage}`})
      })
  }

  // ----- Files -----

  /**
   * see https://docs.gitlab.com/ee/api/repository_files.html#get-raw-file-from-repository
   *
   * sample: @tod gitlab fetch file {k33g/hello-world} {documents/hello.md}
   */

  fetchRawFile({projectId, projectName, nameSpace, filePath, ref }) {
    let id = projectId ? projectId : `${nameSpace}%2F${projectName.replaceAll(".", "%2E").replaceAll('-', '%2D').replaceAll('_', '%5F')}`
    let path = filePath.replaceAll("/", "%2F").replaceAll(".", "%2E").replaceAll('-', '%2D').replaceAll('_', '%5F')

    return this.getRawData({
      path:`/projects/${id}/repository/files/${path}/raw?ref=${ref}`
    })
    .then(response => {
      return response
    })
    .catch(error => { 
      return error
    });
  }

  /*
  Create new file in repository 
  POST /projects/:id/repository/files/:file_path

  file_path (required) - Url encoded full path to new file. Ex. lib%2Fclass%2Erb
  branch (required) - Name of the branch
  start_branch (optional) - Name of the branch to start the new commit from
  encoding (optional) - Change encoding to 'base64'. Default is text.
  author_email (optional) - Specify the commit author's email address
  author_name (optional) - Specify the commit author's name
  content (required) - File content
  commit_message (required) - Commit message


  curl --request POST --header 'PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK' '
  https://gitlab.example.com/api/v4
  /projects/13083/
  repository/files/app%2Fprojectrb%2E?branch=master&author_email=author%40example.com&author_name=Firstname%20Lastname&content=some%20content&commit_message=create%20a%20new%20file'
  */
  createFile({projectId, projectName, nameSpace, filePath, branch, content, commitMessage}) {
    let id = projectId ? projectId : `${nameSpace}%2F${projectName.replaceAll(".", "%2E").replaceAll('-', '%2D').replaceAll('_', '%5F')}`
    let path = filePath.replaceAll("/", "%2F").replaceAll(".", "%2E").replaceAll('-', '%2D').replaceAll('_', '%5F')
    let commit_message = commitMessage.replaceAll("/", "%2F").replaceAll(".", "%2E").replaceAll('-', '%2D').replaceAll('_', '%5F')
    let branch_name = branch.replaceAll("/", "%2F").replaceAll(".", "%2E").replaceAll('-', '%2D').replaceAll('_', '%5F')

    return this.postData({
      path:`/projects/${id}/repository/files/${path}?branch=${branch_name}&commit_message=${commit_message}`, 
      data:{
        content: content,
      }
    })
    .then(response => {
      return response
    })
    .catch(error => { 
      return error
    });
  }

  // ----- Groups -----

  fetchGroups({perPage}) {
    return this.getData({path:`/groups?per_page=${perPage==undefined?20:perPage}`})
  }
  
  fetchGroup({name}) {
    return this.getData({path:`/groups?search=${name}`})
      .then(response => response[0])
      .catch(error => error);
  }

  // ----- Issues -----
  /*
  https://docs.gitlab.com/ee/api/issues.html#new-issue
  POST /projects/:id/issues
  */

  createIssue({projectId, projectName, nameSpace, title, description}) {
    let id = projectId ? projectId : `${nameSpace}%2F${projectName}`
    return this.postData({
      path:`/projects/${id}/issues`, 
      data:{
        title: title,
        description: description
      }
    })
    .then(response => {
      return response
    })
    .catch(error => { 
      return error
    });
  }

  /*
  https://docs.gitlab.com/ee/api/README.html#namespaced-path-encoding
  curl --request GET --header "PRIVATE-TOKEN: xxxxxxxxxx" https://gitlab.com/api/v4/projects/k33g%2Fhello-world
  */

  /*
    https://docs.gitlab.com/ce/api/notes.html#create-new-issue-note
    POST /projects/:id/issues/:issue_iid/notes
      id (required) - The ID or URL-encoded path of the project
      issue_id (required) - The IID of an issue
      body (required) - The content of a note
      created_at (optional) - Date time string, ISO 8601 formatted, e.g. 2016-03-11T03:45:40Z
    curl --request POST --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" https://gitlab.example.com/api/v4/projects/5/issues/11/notes?body=note

  */

  // Create new issue note == answer to the first creation on the issue
  createIssueNote({projectId, issueIId, message}) {
    return this.postData({path:`/projects/${projectId}/issues/${issueIId}/notes`, data:{body:message}})
      .then(response => {
        return response
      })
      .catch(error => { 
        return error
      });
  }

  /**
   * see https://docs.gitlab.com/ee/api/issues.html#list-group-issues
   * GET /groups/:id/issues
   */
  fetchIssuesGroup({groupId, groupName, perPage=20, page=1, state}) {

    let useState = state ? `state=${state}&` : ``

    let id = groupId ? groupId : `${groupName.replaceAll("/", "%2F").replaceAll(".", "%2E").replaceAll('-', '%2D').replaceAll('_', '%5F')}`
    return this.getData({path:`/groups/${id}/issues?${useState}per_page=${perPage}&page=${page}`})
      .then(response => response)
      .catch(error => error);
  }

  // ----- Labels -----


  // https://docs.gitlab.com/ee/api/labels.html

  // POST /projects/:id/labels

  /*
  id	integer/string	yes	The ID or URL-encoded path of the project owned by the authenticated user
  name	string	yes	The name of the label
  color	string	yes	The color of the label given in 6-digit hex notation with leading '#' sign (e.g. #FFAABB) or one of the CSS color names
  description	string	no	The description of the label
  priority	integer	no	The priority of the label. Must be greater or equal than zero or null to remove the priority.
  */

  //.replaceAll("/", "%2F").replaceAll(".", "%2E").replaceAll('-', '%2D').replaceAll('_', '%5F')
  createLabel({projectId, projectName, nameSpace, name, color="azure", description="tbd", priority=0}) {
    let id = projectId ? projectId : `${nameSpace.replaceAll("/", "%2F").replaceAll(".", "%2E").replaceAll('-', '%2D').replaceAll('_', '%5F')}%2F${projectName}`
    console.log(id)
    return this.postData({
      path:`/projects/${id}/labels`, 
      data:{
        name: name,
        color: color,
        description: description,
        priority: priority
      }
    })
    .then(response => {
      return response
    })
    .catch(error => { 
      return error
    });
  }

  // ----- MR -----

  /*
    https://docs.gitlab.com/ee/api/merge_request_approvals.html
    POST /projects/:id/merge_requests/:merge_request_iid/approve
  */

  approveMergeRequest({projectId, mergeRequestIid}) {
    
    return this.postData({
      path:`/projects/${projectId}/merge_requests/${mergeRequestIid}/approve`, 
      data:{}
    })
    .then(response => {
      return response
    })
    .catch(error => { 
      return error
    });
  }

  //POST /projects/:id/merge_requests/:merge_request_iid/notes
  createMergeRequestNote({projectId, mergeRequestIId, message}) {
    return this.postData({path:`/projects/${projectId}/merge_requests/${mergeRequestIId}/notes`, data:{body:message}})
      .then(response => {
        console.log("🤩",response)
        return response
      })
      .catch(error => { 
        return error
      });
  }

  // ----- PayLoad -----

  getUserFromPayLoad({payload}) {
    return {
      name: payload.user.name,
      username: payload.user.username
    }
  }
  
  getIssueFromPayLoad({payload}) {
    return { // object_attributes:
      id: payload.object_attributes.id,
      iid: payload.object_attributes.iid,
      description: payload.object_attributes.description, // the message
      author_id: payload.object_attributes.author_id,
      title: payload.object_attributes.title,
      action: payload.object_attributes.action,
      state:  payload.object_attributes.state
    }
  }
  
  getProjetFromPayLoad({payload}) {
    return {
      id: payload.project.id,
      name: payload.project.name,
      description: payload.project.description,
      web_url: payload.project.web_url
    }
  }
  
  getRepositoryFromPayLoad({payload}) {
    return {
      name: payload.repository.name,
      url: payload.repository.url,
      description: payload.repository.description,
      homepage: payload.repository.homepage
    }
  }
  
  getNoteFromPayLoad({payload}) {
  
    let note = { // object_attributes:
      id: payload.object_attributes.id,
      noteable_type: payload.object_attributes.noteable_type,
      noteable_id: payload.object_attributes.noteable_id,
      project_id: payload.object_attributes.project_id,
      note: payload.object_attributes.note, // the message
      author_id: payload.object_attributes.author_id,
      discussion_id: payload.object_attributes.discussion_id
    }
  
    if(note.noteable_type=="MergeRequest") {
      note.merge_request = payload.merge_request
    }
  
  
    return note
  }
  
  getIssueFromNotePayLoad({payload}) {
    return { // object_attributes:
      id: payload.issue.id,
      iid: payload.issue.iid,
      description: payload.issue.description, // the message
      author_id: payload.issue.author_id,
      title: payload.issue.title,
      state:  payload.issue.state
    }
  }
  
  getMergeRequestFromNotePayLoad({payload}) {
    return { 
      id: payload.merge_request.id,
      iid: payload.merge_request.iid,
      description: payload.merge_request.description, // the message
      author_id: payload.merge_request.author_id,
      title: payload.merge_request.title,
      state:  payload.merge_request.state
    }
  }
  
  getJobFromPayLoad({payload}) {
    return {
      object_kind: payload.object_kind,
      ref: payload.ref,
      tag: payload.tag,
      build_name: payload.build_name,
      build_stage: payload.build_stage,
      build_status: payload.build_status,
      project_id: payload.project_id,
      project_name: payload.project_name
    }
  }
  
  getPipeLineFromPayLoad({payload}) {
    return {
      object_kind: payload.object_kind,
      ref: payload.object_attributes.ref,
      tag: payload.object_attributes.tag,
      status: payload.object_attributes.status,
      stages: payload.object_attributes.stages,
      created_at: payload.object_attributes.created_at,
      finished_at: payload.object_attributes.finished_at,
      duration: payload.object_attributes.duration,
      user: payload.user,
      project: payload.project,
      builds: payload.builds
    }
  }  


  getGitLabContext({payload}) {

    let user = this.getUserFromPayLoad({payload}) // you can check the user (to avoid loop answers)
    let project = this.getProjetFromPayLoad({payload})
    
  
    //TODO: REFACTOR THAT!!!
    var note = payload.issue ? this.getNoteFromPayLoad({payload}) : undefined
    var issue = note ? this.getIssueFromNotePayLoad({payload}) : this.getIssueFromPayLoad({payload})
    var mergeRequest
  
  
    if(payload.object_kind=="note") {
      note = this.getNoteFromPayLoad({payload})
  
      if(payload.merge_request) {
        mergeRequest = this.getMergeRequestFromNotePayLoad({payload})
        //console.log("🤖", mergeRequest)
        issue = undefined // if it's a merge request, it's not an issue
      }
  
    }
    
    return {
      isSourceControlContext: true, // don't remove the Chat trigger from the text issue or note
      isChatContext: false,
      user: () => user.username,
      message: () => {
        if(note) {
          return note.note
        }
        if(issue) {
          if(issue.description) {
            return issue.description
          } else {
            return ""
          }
          
        }
        if(mergeRequest) {
          return mergeRequest.description
        }
      },
      channel: () => project.name,
      send: (message) => { // create an issue
        // TODO: foo 
      },
      answer: (message) => { // reply to an issue
  
        if(issue) {
          return this.createIssueNote({projectId: project.id, issueIId: issue.iid, message:message })
        }
  
        if(mergeRequest) {
          return this.createMergeRequestNote({projectId: project.id, mergeRequestIId: mergeRequest.iid, message:message })
        }
  
      }
    }
  }





}

module.exports = GitLabClient
