console.log("> 🤖 using Slack middleware")

const fetch = require('node-fetch')

//TODO: test here the environment variables

/* === Slack === 
Incoming WebHook
payload={
  "text": "This is a line of text in a channel.\nAnd this is another line of text.
"}

Send message test:
curl -X POST --data-urlencode "payload={\"channel\": \"#random\", \"username\": \"webhookbot\", \"text\": \"This is posted to #random and comes from a bot named webhookbot.\", \"icon_emoji\": \":ghost:\"}" https://hooks.slack.com/services/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

*/

function incomingWebHook() {
  return process.env.INCOMING_WEBHOOK
  //TODO make a test
}

function message() {
  return this.request.body.text
}

function token() {
  return this.request.body.token
}

function checkAuthenticationToken() {
  return this.token() == process.env.CHAT_TOKEN
}

function user() {
  return {
    name: this.request.body.user_name,
    id: this.request.body.user_id
  }
}

function channel() {
  return {
    name: this.request.body.channel_name,
    id: this.request.body.channel_id
  }
}

function send(message) {
  // TODO ow to send to a specific room
  console.log("> message", message)

  let bodyData = JSON.stringify({
    "text": message,
    "username": process.env.BOT_NAME || "Tod",
    "icon_url": process.env.BOT_AVATAR || null
  })

  return fetch(this.incomingWebHook(), {
    method: 'POST',
    headers: {
      "Content-Type": "application/json;utf-8"
    },
    body: bodyData
  })
  .then(response => response)
  .catch(err => console.log(`😡 Context.send`, err))
}

module.exports = {
  send: send,
  channel: channel,
  user: user,
  token: token,
  message: message,
  incomingWebHook: incomingWebHook,
  checkAuthenticationToken: checkAuthenticationToken
};