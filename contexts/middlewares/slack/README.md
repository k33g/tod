# Slack setup

## Integrations

### Incoming Webhook

- go to **Incoming Webhook**
- click on **Add Configuration**
- select a `Channel` (eg *random*)
- click on **Add Incoming Webhook**
- copy the webhook url and setup the environment variable `INCOMING_WEBHOOK` with it

### Outgoing Webhook

- go to **Outgoing Webhook**
- click on **Add Configuration**
- click on **Add Outgoing Webhook**
- select *Any* for `Channel` 
- add `Trigger Words ` (eg *@tod*)
- add a `Callback URL` (http://bot_domain:port/hey/tod) *the url always ends with `hey/tod`*
- set the environment variable `CHAT_TOKEN` with the given token

## Start Tod the bot

Now you can start your bot like that:

```shell
#!/bin/sh
BOT_PORT=9090 \
BOT_MIDDLEWARE="slack/slack.js" \
BOT_BRAIN="actions-sandbox" \
BOT_TRIGGER="@tod" \
BOT_NAME="tod" \
BOT_AVATAR="http://bot_domain:port/foxy.png" \
INCOMING_WEBHOOK="http://slack_domain/hooks/..." \
CHAT_TOKEN="slack_token" \
npm start
```

> Remarks: 
> - you can change the bot's name with `BOT_NAME` eand the bot's avatar with `BOT_AVATAR`
> - `BOT_BRAIN="actions"` loads the features of your bot (see samples `actions.js` and `actions-sandbox.js` at the root of the project)