# RocketChat setup

## Admin setup

- connect as root
- go to **Administration** section
- create a new user `tod` (add an email, set `Verified` to `true`, add the `bot` role, set `Join default channels` to `true`)
- connect to the user bot to set an avatar

Then,

- go to **Integrations** section
- click on **New Integration** button

## Integrations

### Incoming Webhook

- click on **Incoming Webhook**
- set `Enabled` to `true`
- add a `Channel` (eg *#general*)
- set the username (eg *Tod*) in `Post as` field *(The user must already exist)*
- copy the webhook url and setup the environment variable `INCOMING_WEBHOOK` with it
- click on the **Save Changes** button

### Outgoing Webhook

- click on **Outgoing Webhook**
- choose the `Event Trigger`: *Message Sent*
- set `Enabled` to `true`
- add a `Channel` (eg *#general*)
- add `Trigger Words ` (eg *@tod*)
- add a `Callback URL` (http://bot_domain:port/hey/tod) *the url always ends with `hey/tod`*
- set `Impersonate User` to `true`
- set the environment variable `CHAT_TOKEN` with the given token
- click on the **Save Changes** button


## Start Tod the bot

Now you can start your bot like that:

```shell
#!/bin/sh
BOT_PORT=9090 \
BOT_MIDDLEWARE="rocketchat/rocketchat.js" \
BOT_BRAIN="actions-sandbox" \
BOT_TRIGGER="@tod" \
BOT_NAME="tod" \
INCOMING_WEBHOOK="http://rocketchat-domain/hooks/..." \
CHAT_TOKEN="rocketchat_token" \
npm start
```

> Remarks: 
> - `BOT_BRAIN="actions"` loads the features of your bot (see samples `actions.js` and `actions-sandbox.js` at the root of the project)