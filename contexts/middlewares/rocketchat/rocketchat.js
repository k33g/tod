console.log("> 🤖 using RocketChat middleware")

const fetch = require('node-fetch')

//TODO: test here the environment variables

function incomingWebHook() {
  return process.env.INCOMING_WEBHOOK
  //TODO make a test
}

function message() {
  return this.request.body.text
}

function token() {
  return this.request.body.token
}

function checkAuthenticationToken() {
  return this.token() == process.env.CHAT_TOKEN
}

function user() {
  return {
    name: this.request.body.user_name,
    id: this.request.body.user_id
  }
}

function channel() {
  return {
    name: this.request.body.channel_name,
    id: this.request.body.channel_id
  }
}


function send(message) {

  console.log("> message", message)

  /*
curl -X POST --data-urlencode "payload={\"username\": \"tod\", \"text\": \"hello\"}" http://rocketchat.local:3000/hooks/o27g8MYqsdedMMhuB/MGFGCkt6AKojctMSbqYs8GYaJ8Lky6RGrDjankg4DcdGsBBs
  



  let bodyData = JSON.stringify({
    "text": message,
    "username": process.env.BOT_NAME || "Tod",
    "icon_url": process.env.BOT_AVATAR || null
  })
  */

  let bodyData = JSON.stringify({
    "text": message
  })

  return fetch(this.incomingWebHook(), {
    method: 'POST',
    headers: {
      "Content-Type": "application/json"
    },
    body: bodyData
  })
  .then(response => response)
  .catch(err => console.log(`😡 Context.send`, err))
}

module.exports = {
  send: send,
  channel: channel,
  user: user,
  token: token,
  message: message,
  incomingWebHook: incomingWebHook,
  checkAuthenticationToken: checkAuthenticationToken
};