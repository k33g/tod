console.log("> 🤖 using Mattermost middleware")

const fetch = require('node-fetch')

//TODO: test here the environment variables

/* === MatterMost ===
{ channel_id: 'unwsmwf1wty5583wsiozcyt1jc',
  channel_name: 'town-square',
  file_ids: '',
  post_id: 'u3um1uy41tgbpfuzdqatxuiebw',
  team_domain: 'acme',
  team_id: 'zkmwn5q6dirf5ed4nhspfmx74h',
  text: '#hi give price clever cloud',
  timestamp: '1519504516',
  token: 'qfurzrxy9jbwpr4on7o34jratr',
  trigger_word: '#hi',
  user_id: 'mkdfx3mop3dq8gsykdkw55xehe',
  user_name: 'k33g' }
*/

function incomingWebHook() {
  return process.env.INCOMING_WEBHOOK
  //TODO make a test
}

function message() {
  return this.request.body.text
}

function token() {
  return this.request.body.token
}

function checkAuthenticationToken() {
  return this.token() == process.env.CHAT_TOKEN
}

function user() {
  return {
    name: this.request.body.user_name,
    id: this.request.body.user_id
  }
}

function channel() {
  return {
    name: this.request.body.channel_name,
    id: this.request.body.channel_id
  }
}

function send(message) {
  // https://docs.mattermost.com/developer/webhooks-incoming.html
  // you can override the channel:
  // https://docs.mattermost.com/developer/webhooks-incoming.html#override-the-channel
  console.log("> message", message)

  let bodyData = JSON.stringify({
    "text": message,
    "username": process.env.BOT_NAME || "Tod",
    "icon_url": process.env.BOT_AVATAR || null
  })

  return fetch(this.incomingWebHook(), {
    method: 'POST',
    headers: {
      "Content-Type": "application/json;utf-8"
    },
    body: bodyData
  })
  .then(response => response)
  .catch(err => console.log(`😡 Context.send`, err))
}

module.exports = {
  send: send,
  channel: channel,
  user: user,
  token: token,
  message: message,
  incomingWebHook: incomingWebHook,
  checkAuthenticationToken: checkAuthenticationToken
};