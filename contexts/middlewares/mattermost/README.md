# Mattermost setup

> GitLab Mattermost install

## Admin setup

- connect as root (as GitLab administrator)
- go to **INTEGRATIONS/Custom Integrations** section
  - set **Enable Incoming Webhooks** to `true`
  - set **Enable Outgoing Webhooks** to `true`
  - set **Enable integrations to override usernames** to `true`
  - set **Enable integrations to override profile picture icons** to `true`
  - set **Enable Personal Access Tokens** to `true`
- click on **Save** button

## Integrations

- connect as user
- select **Integrations** in the user menu

### Incoming Webhook

- click on **Incoming Webhook**
- click on **Add Incoming Webhook**
- add a `Title` eg *Tod talks to Mattermost*
- add a `Description`
- select a `Channel` (eg *Town Square*)
- copy the webhook url and setup the environment variable `INCOMING_WEBHOOK` with it
- click on the **Done** button

### Outgoing Webhook

- click on **Outgoing Webhook**
- click on **Add Outgoing Webhook**
- add a `Title` eg *Mattermost talks to Tod*
- add a `Description`
- select `application/json` for the `Content Type` value
- select a `Channel` (eg *Town Square*)
- add `Trigger Words ` (eg *@tod*)
- choose *First word starts wit ha trigger word* for the `Trigger When` value 
- add a `Callback URL` (http://bot_domain:port/hey/tod) *the url always ends with `hey/tod`*
- click on the **Save** button
- set the environment variable `CHAT_TOKEN` with the given token
- click on the **Done** button

## Start Tod the bot

> :warning: if you are on a local demo,  use tool like ngronk.

Now you can start your bot like that:

```shell
#!/bin/sh
BOT_PORT=9090 \
BOT_MIDDLEWARE="mattermost/mattermost.js" \
BOT_BRAIN="actions" \
BOT_TRIGGER="@tod" \
BOT_NAME="tod" \
BOT_AVATAR="http://bot_domain:port/foxy.png" \
INCOMING_WEBHOOK="http://mattermost_domain/hooks/..." \
CHAT_TOKEN="mattermost_token" \
npm start
```

> Remarks: 
> - you can change the bot's name with `BOT_NAME` eand the bot's avatar with `BOT_AVATAR`
> - `BOT_BRAIN="actions"` loads the features of your bot (see samples `actions.js` and `actions-sandbox.js` at the root of the project)