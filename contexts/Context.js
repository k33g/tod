
class Context {
  constructor({request, response}, ...features) {
    this.request = request
    this.response = response
    this.isSourceControlContext = false // don't remove the Chat trigger from the text issue or note
    this.isChatContext = true
    return Object.assign(this, ...features)
  }

}

module.exports = Context

