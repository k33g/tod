console.log("> 🤖 adding features to the context | features")

function hello() { return "👋 hello" }

function hi() { return "🖖 hi" }

module.exports = {
  hello: hello,
  hi: hi
};