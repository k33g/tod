console.log("> 🤖 adding features to the context | hooks")

function headers() {
  return this.request.headers
}

function body() {
  return this.request.body
}

module.exports = {
  headers: headers,
  body: body
};