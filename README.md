# Tod 🦊 the 🤖

Tod is a multi chat bot, currently he can work with:

- Slack
- Mattermost (GitLab version)
- RocketChat
- :construction: Twitter
- :construction: soon GitLab
- :construction: soon GitHub

## Start Tod

### Slack

```shell
BOT_PORT=9090 \
BOT_MIDDLEWARE="slack/slack.js" \
BOT_BRAIN="actions-sandbox" \
BOT_TRIGGER="@tod" \
BOT_NAME="tod" \
BOT_AVATAR="http://bot_domain:port/foxy.png" \
INCOMING_WEBHOOK="http://slack_domain/hooks/..." \
CHAT_TOKEN="slack_token" \
npm start
```

See how to setup Tod and Slack [contexts/middlewares/slack/README.md](contexts/middlewares/slack/README.md)

### Mattermost

```shell
BOT_PORT=9090 \
BOT_MIDDLEWARE="mattermost/mattermost.js" \
BOT_BRAIN="actions" \
BOT_TRIGGER="@tod" \
BOT_NAME="tod" \
BOT_AVATAR="http://bot_domain:port/foxy.png" \
INCOMING_WEBHOOK="http://mattermost_domain/hooks/..." \
CHAT_TOKEN="mattermost_token" \
npm start
```

See how to setup Tod and Mattermost [contexts/middlewares/mattermost/README.md](contexts/middlewares/mattermost/README.md)

### RocketChat

```shell
BOT_PORT=9090 \
BOT_MIDDLEWARE="rocketchat/rocketchat.js" \
BOT_TRIGGER="@tod" \
BOT_NAME="tod" \
BOT_BRAIN="actions-sandbox" \
INCOMING_WEBHOOK="http://rocketchat-domain/hooks/..." \
CHAT_TOKEN="rocketchat_token" \
npm start
```

See how to setup Tod and RocketChat [contexts/middlewares/rocketchat/README.md](contexts/middlewares/rocketchat/README.md)


## Add actions to Tod

- see `./actions-sandbox.js` or `./actions-sandbox.js`
- write your own features in `./your_features.js` and set the environment variable to `BOT_BRAIN="your_features" ` (without `./` and `.js`)
- then you can run Tod

## Write actions for Tod

> WIP :construction:

An action for **Tod** is "an answer to a question". That is to say: if **Tod** hears and recognizes a question, it will answer you. For example, create an "action" file somewhere in the project (eg: `actions.js`) with this structure:

```javascript
module.exports =  ({robot, context, scm}) =>  {
  //👋 add the actions here
}
```

Add the actions, save the file, setup the `BOT_BRAIN` environment variable with the name of the file (without `./` and `.js`) and run **Tod**

### Add an action

Currently, **Tod** has 5 ways to hear (and soon more):

- `hearExactOrderWords`: you have to write the words in the expected order
- `hearRandomOrderWords`: what ever the order
- `hearJWDistance`: **Tod** calculates the **Jaro-Winkler distance** between what you typed and what it expects, before answering
- `hearLDistance`: **Tod** calculates the **Levenshtein distance** between what you typed and what it expects, before answering
- `hearDiceCoefficient`: **Tod** calculates the **Dice's coefficient** between what you typed and what it expects, before answering

#### hearExactOrderWords sample

```javascript
robot.hearExactOrderWords(
  {sentence: "hello how are you doing", context: context}, 
  ({message, channel, user, tokens, ngrams}) => {
    robot.send({
      message: `:wave: Hey ${user.name} I'm fine than you :tada: [from ${channel.name}]`, 
      context: context
    })
  }
)
```

#### hearRandomOrderWords sample

```javascript
robot.hearRandomOrderWords(
  {sentence: "give price me", context: context}, 
  ({message, channel, user, tokens, ngrams}) => {
    let answer = `100 :euro: ${user.name}, this is the pricing`
    robot.send({
      message: answer, 
      context: context
    })
  }
)
```

#### hearJWDistance sample

```javascript
// similarityTrigger (default value is 0.8) 
// if the Jaro-Winkler distance is > to the similarityTrigger then the robot answers
robot.hearJWDistance(
  {sentence: "how are you", context: context, similarityTrigger: 0.7}, 
  ({message, channel, user, jaroWinklerDistance}) => {

    robot.send({
      message: `> distance [Jaro-Winkler]: ${jaroWinklerDistance} \n> :wave: ${user.name} I'm fine :relaxed:`, 
      context: context
    })
  }
) 
```

#### hearLDistance sample

```javascript
// similarityTrigger (default value is 5) 
// if Levenshtein distance is <= to similarityTrigger then the robot answers
robot.hearLDistance(
  {sentence: "what up buddy", context: context, similarityTrigger: 5}, 
  ({message, channel, user, levenshteinDistance}) => {

    robot.send({
      message: `> distance [Levenshtein]: ${levenshteinDistance} \n> :wave: ${user.name} the weather is sunny :sunny:`, 
      context: context
    })
  }
) 
```

#### hearDiceCoefficient sample

```javascript
// similarityTrigger (default value is 0.75) 
// if Dice's coefficient is < to similarityTrigger then the robot answers
robot.hearDiceCoefficient(
  {sentence: "what your real name", context: context, similarityTrigger: 0.75}, 
  ({message, channel, user, diceCoefficient}) => {

    robot.send({
      message: `> Coefficient [Dice]: ${diceCoefficient} \n> :wave: ${user.name}, I'm Tod The Bot akka :fox_face: :robot_face:`, 
      context: context
    })
  }
) 
```

## Web Hooks

You can use **web hooks** with **TOD**. You only have to create a `POST` request on this url: `http(s)://<your_domain>:<port>/hooks/tod` with **JSON data**, for example:

```shell
curl -H "Content-Type: application/json" -X POST -d '{"user":"bob morane"}' http://botsgarden.eu.ngrok.io/hooks/tod
```

### How to cach the hook event?

You have to create a file, eg: `hook.js`:

```javascript
module.exports =  ({robot, context, scm}) =>  {

  robot.onHookEvent({context: context}, ({headers, payload}) => {

    robot.send({
      message: `:wave: web hook event happened`, 
      context: context
    })
  })

}
```

:warning: you have to setup the environment variable `BOT_HOOKS="hooks"` with the name of the file (without `./` and `.js`) and run **Tod**.

### GitLab Project Hook Events

> WIP :construction: hook groups

You can setup a webhook in your GitLab project (see: https://docs.gitlab.com/ce/user/project/integrations/webhooks.html)

If you added a secret token to the hook, you must setup the environment variable `GITLAB_HOOK_TOKEN="your-secret-token"`.

Ans you can catch the GitLab Hook Events like that:

```javascript
module.exports =  ({robot, context, scm}) =>  {

  robot.onGitLabEvent({context: context}, 
    ({headers, payload}) => {

      robot.send({
        message: `:fox_face: host: ${headers["host"]} \n event: ${headers["x-gitlab-event"]}`, 
        context: context
      })
    }
  )
}
```

Or more specifically, eg for an issue:

```javascript
module.exports =  ({robot, context, scm}) =>  {

  robot.onGitLabIssue({context: context}, 
    ({headers, payload}) => {

      robot.send({
        message: `:fox_face: Issue by ${payload.user.name} | ${payload.user.username}`, 
        context: context
      })
    }
  )
}
```

Here are the events you can use:

- `onGitLabEvent`
- `onGitLabIssue`
- `onGitLabConfidentialIssue`
- `onGitLabNote`
- `onGitLabPush`
- `onGitLabTagPush`
- `onGitLabMergeRequest`
- `onGitLabPipeLine`
- `onGitLabJob`
- `onGitLabWikiPage`

### GitHub Repository Hook Events

> WIP :construction: hook organizations

You can setup a webhook in your GitHun repository (see: https://developer.github.com/webhooks/)

If you added a secret token to the hook, you must setup the environment variable `GITHUB_HOOK_TOKEN="your-secret-token"`.

Ans you can catch the GitHub Hook Events like that:

```javascript
module.exports =  ({robot, context, scm}) =>  {

  robot.onGitHubEvent({context: context}, 
    ({headers, payload}) => {

      robot.send({
        message: `:octopus: host: ${headers["host"]} \n event: ${headers["x-github-event"]}`, 
        context: context
      })
    }
  )

}
```

Or more specifically, eg for an issue:

```javascript
module.exports =  ({robot, context, scm}) =>  {

  robot.onGitHubIssue({context: context}, 
    ({headers, payload}) => {

      robot.send({
        message: `:octopus: Issue by ${payload.sender.login}`, 
        context: context
      })
    }
  )
}
```

Here are the events you can use:

- `onGitHubEvent`
- `onGitHubIssue`
- ...

> WIP :construction:


## Add features

> WIP :construction:

### Add features to Context

### Add features to Robot

## Add middlewares

> WIP :construction:


## TODO

- logs
- memory
- GitLab API - interactions
- GitHub API - interactions
- GitBucket API - interactions
- ...