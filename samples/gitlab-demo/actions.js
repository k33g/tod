console.log("> 🤖 brain | actions: gitlab/sandbox")

const GitLabClient = require('../../scm/gitlab/GitLabClient')

let gitLabTodCli = new GitLabClient({
  baseUri: process.env.GITLAB_INSTANCE_URL, 
  token: process.env.TOD_PERSONAL_ACCESS_TOKEN
})


module.exports =  ({robot, context}) =>  {

  robot.hearRandomOrderWords(
    {sentence: "how are you", context: context}, 
    ({message, channel, user, tokens, ngrams}) => {
      robot.send({
        message: `👋 Hey ${user.name} from ${channel.name}`, 
        context: context
      })
    }
  )
  // @tod gitlab user info k33g
  robot.hearRandomOrderWords(
    {sentence: "gitlab user info", context: context}, 
    ({message, channel, user, tokens}) => {

      console.log("😀 message", message)
      console.log("😀 tokens", tokens) //take the last token to get the handel

      let handle = tokens.slice(-1)[0]

      console.log("🦊 handle", handle)
      
      gitLabTodCli.fetchUserByHandle(handle)
        .then(data => {

          robot.send({
            message: robot.formatText(`
              :wave: Hey ${user.name} so you want some information about ${handle} ...
              Here are the informations:
              > id: ${data.username}
              > name: ${data.name}
              > state: ${data.state}
              > avatar_url: ${data.avatar_url}
              > web_url: ${data.web_url}
            `), 
            context: context
          })
        })
        .catch(err => {
          robot.send({
            message: robot.formatText(`
              :wave: Hey ${user.name} so you want some information about ${handle} ...
              But sorry :crying_cat_face: I can't find any information
            `), 
            context: context
          })
        }) // end of gitLabCli.fetchUserByHandle
    }
  )

}