console.log("> 🤖 brain | hooks: gitlab/sandbox")

const GitLabClient = require('../../scm/gitlab/GitLabClient')

let tod = new GitLabClient({
  baseUri: process.env.GITLAB_INSTANCE_URL, 
  token: process.env.TOD_PERSONAL_ACCESS_TOKEN
})

let buster = new GitLabClient({
  baseUri: process.env.GITLAB_INSTANCE_URL, 
  token: process.env.BUSTER_PERSONAL_ACCESS_TOKEN
})

let babs = new GitLabClient({
  baseUri: process.env.GITLAB_INSTANCE_URL, 
  token: process.env.BABS_PERSONAL_ACCESS_TOKEN
})

/*
const GitLab = require('../../scm/gitlab/GitLab')

let tod = GitLab.getClient({
  baseUri:"http://gitlab-ee.local/api/v4", 
  token: process.env.TOD_PERSONAL_ACCESS_TOKEN
})

let buster = GitLab.getClient({
  baseUri:"http://gitlab-ee.local/api/v4", 
  token: process.env.BUSTER_PERSONAL_ACCESS_TOKEN
})

let babs = GitLab.getClient({
  baseUri:"http://gitlab-ee.local/api/v4", 
  token: process.env.BABS_PERSONAL_ACCESS_TOKEN
})
*/

module.exports =  ({robot, context}) =>  {

  robot.onHookEvent({context: context}, ({headers, payload}) => {
    console.log(headers["x-gitlab-event"])
  })

  // this is the context chat, if you want to discuss in the issues
  // you have to create a "Gitlab Context"
  robot.onGitLabIssue({context: context}, ({headers, payload}) => {
    
    console.log("😍 onGitLabIssue")
    
    let issue = tod.getIssueFromPayLoad({payload})
    let issueUser = tod.getUserFromPayLoad({payload})

    console.log("👋", issueUser)
    
    // use an other context than the context chat to speak with GitLab
    let busterContext = buster.getGitLabContext({payload: payload})
    let babsContext = babs.getGitLabContext({payload: payload})
    let todContext = tod.getGitLabContext({payload: payload})

    console.log("before hearDiceCoefficient")

    

    robot.hearDiceCoefficient({sentence: "@busterbunny69 hi", context: busterContext}, ({message, channel, user, diceCoefficient}) => {
      
      console.log("4 ========", "getGitLabContext", "========")
      
      robot.answer({
        message: `:wave: ${user}`, 
        context: busterContext
      })

      robot.send({
        message: robot.formatText(`
          > Issue: ${issue.id}/${issue.iid} *${issue.title}* / **${issue.state}**
          > User: ${issueUser.username}
        `), 
        context: context // chat context
      })
      
    })

    robot.hearDiceCoefficient({sentence: "@babsbunny42 hi", context: babsContext}, ({message, channel, user, diceCoefficient}) => {
      robot.answer({
        message: `:wave: ${user}`, 
        context: babsContext
      })

      robot.send({
        message: robot.formatText(`
          > Issue: ${issue.id}/${issue.iid} *${issue.title}* / **${issue.state}**
          > User: ${issueUser.username}
        `), 
        context: context // chat context
      })
      
    })

    // hearDiceCoefficient
    robot.hearWordsInTheMessage({sentence: "@all hi", context: todContext}, ({message, channel, user, diceCoefficient}) => {

      robot.answer({
        message: `:robot_face: ok ${user}`, 
        context: todContext
      })
      robot.answer({
        message: `:tea: ${user}`, 
        context: babsContext
      })
      robot.answer({
        message: `:coffee: ${user}`, 
        context: busterContext
      })

      robot.send({
        message: robot.formatText(`
          > Issue: ${issue.id}/${issue.iid} *${issue.title}* / **${issue.state}**
          > User: ${issueUser.username}
        `), 
        context: context // chat context
      })
      

    })

    //let user = gitLabTodCli.getUserFromPayLoad({payload}) 
    //let project = gitLabTodCli.getProjetFromPayLoad({payload})
    
  }) // end of robot.onGitLabIssue

  robot.onGitLabNote({context: context}, ({headers, payload}) => {
    console.log("😀 onGitLabNote")
    let note = tod.getNoteFromPayLoad({payload})
    //console.log(note)

    // use an other context than the context chat to speak with GitLab
    let busterContext = buster.getGitLabContext({payload: payload})
    let babsContext = babs.getGitLabContext({payload: payload})


    if(note.merge_request) {
      //console.log("note.merge_request.iid", note.merge_request.iid)
      //console.log("note.project_id", note.project_id)

      robot.hearDiceCoefficient({sentence: "@busterbunny69 hi", context: busterContext}, ({message, channel, user, diceCoefficient}) => {

        robot.answer({
          message: `:wave: ${user}`, 
          context: busterContext
        })
      })

      robot.hearDiceCoefficient({sentence: "@busterbunny69 please review", context: busterContext}, ({message, channel, user, diceCoefficient}) => {

        buster.approveMergeRequest({projectId:note.project_id, mergeRequestIid: note.merge_request.iid})

        robot.send({
          message: robot.formatText(`
            > :wave: @busterbunny69 is approving MR ${note.merge_request.iid}
            > :thumbs_up: ${user} great feature
          `), 
          context: context // chat context
        })

        robot.answer({
          message: `:thumbs_up: ${user} great feature`, 
          context: busterContext
        })
      })

      robot.hearDiceCoefficient({sentence: "@babsbunny42 please review", context: babsContext}, ({message, channel, user, diceCoefficient}) => {

        babs.approveMergeRequest({projectId:note.project_id, mergeRequestIid: note.merge_request.iid})

        robot.send({
          message: robot.formatText(`
            > :wave: @babsbunny42 is approving MR ${note.merge_request.iid}
            > :blue_heart: ${user} great feature :sparkles:
          `), 
          context: context // chat context
        })

        robot.answer({
          message: `:blue_heart: ${user} great feature :sparkles:`, 
          context: babsContext
        })
      })

    } else {

      robot.hearDiceCoefficient({sentence: "@busterbunny69 hi", context: busterContext}, ({message, channel, user, diceCoefficient}) => {

        robot.answer({
          message: `:wave: ${user}`, 
          context: busterContext
        })
      })

    }

  })
  /* not on a merge request
  😀 onGitLabNote
  { id: 60,
    noteable_type: 'MergeRequest',
    noteable_id: 8,
    project_id: 2,
    note: '@babsbunny42 can you read this',
    author_id: 2,
    discussion_id: '522b9e1cf83a4133bd3cf113c0d418c8f543f495' }
  */


  robot.onGitLabMergeRequest({context: context}, ({headers, payload}) => {
    console.log("😎 onGitLabMergeRequest")
    let issue = tod.getIssueFromPayLoad({payload}) // try with getMergeRequestFromPayload
    let user = tod.getUserFromPayLoad({payload})
    console.log(issue)
    robot.send({
      message: robot.formatText(`
        > Merge Request: ${issue.id}/${issue.iid} *${issue.title}* / **${issue.state}**
        > User: ${user.username}
      `), 
      context: context // chat context
    })

  })
  /* a MR is an issue
    😎 onGitLabMergeRequest
    { id: 9,
      iid: 2,
      description: 'Closes #3',
      author_id: 2,
      title: 'WIP: Resolve "add a great feature"',
      action: 'open',
      state: 'opened' }
  */

  robot.onGitLabJob({context: context}, ({headers, payload}) => {
    console.log("😎 onGitLabJob")
    let job = tod.getJobFromPayLoad({payload})
    console.log(job)

    robot.send({
      message: robot.formatText(`
        > Job: ${job.object_kind} on ref: ${job.ref} / **${job.project_name}**
        > Build name: ${job.build_name}
        > Build stage: ${job.build_stage}
        > Build status: ${job.build_status} ${job.build_status=="success" ? "🍾🎆✨" : "" } ${job.build_status=="running" ? "⏳🤔" : "" }
      `), 
      context: context // chat context
    })

  })

  robot.onGitLabPipeLine({context: context}, ({headers, payload}) => {
    console.log("😎 onGitLabPipeLine")
    let pipeLine = tod.getPipeLineFromPayLoad({payload})
    console.log(pipeLine)

    robot.send({
      message: robot.formatText(`
        > Pipeline: ${pipeLine.object_kind} on ref: ${pipeLine.ref} / **${pipeLine.project.name}**
        > Build stages: ${pipeLine.stages}
        > Build status: ${pipeLine.status}
        >   created at: ${pipeLine.created_at}
        >   finished at: ${pipeLine.finished_at}
        >   duration: ${pipeLine.duration}
        > *By ${pipeLine.user.name}*
      `), 
      context: context // chat context
    })

  })

}
