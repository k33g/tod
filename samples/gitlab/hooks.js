console.log("> 🤖 brain | GitLab hooks")

module.exports =  ({robot, context}) =>  {

  robot.onHookEvent({context: context}, ({headers, payload}) => {

    console.log("👋", "something happened", headers["x-gitlab-event"])
    robot.send({
      message: `:wave: web hook event happened ${headers["x-gitlab-event"]}`, 
      context: context
    })

  })

  robot.onGitLabEvent({context: context}, 
    ({headers, payload}) => {

      console.log("🦊", headers["host"], "event:", headers["x-gitlab-event"])

      robot.send({
        message: `:fox_face: host: ${headers["host"]} \n event: ${headers["x-gitlab-event"]}`, 
        context: context
      })

    }
  )

  robot.onGitLabIssue({context: context}, 
    ({headers, payload}) => {

      console.log("🦊 Issue", payload.user.name, payload.user.username)

      robot.send({
        message: `:fox_face: Issue by ${payload.user.name} | ${payload.user.username}`, 
        context: context
      })

    }
  )

  robot.onGitLabNote({context: context}, 
    ({headers, payload}) => {

      console.log("🦊 Issue Comment", payload.user.name, payload.user.username)

      robot.send({
        message: `:fox_face: Issue Comment by ${payload.user.name} | ${payload.user.username}`, 
        context: context
      })

    }
  )


  robot.onGitLabPush({context: context}, 
    ({headers, payload}) => {

      console.log("🦊 Push on", payload.project.name)

      robot.send({
        message: `:fox_face: Push on: ${ payload.project.web_url}`, 
        context: context
      })

    }
  )

  robot.onGitLabTagPush({context: context}, 
    ({headers, payload}) => {

      console.log("🦊 Tag Push on", payload.project.name)

      robot.send({
        message: `:fox_face: Tag Push on: ${ payload.project.web_url}`, 
        context: context
      })

    }
  )  

  robot.onGitLabMergeRequest({context: context}, 
    ({headers, payload}) => {

      console.log("🦊 Merge Request on", payload.project.name)

      robot.send({
        message: `:fox_face: Merge Request on: ${ payload.project.web_url}`, 
        context: context
      })

    }
  )  

  robot.onGitLabWikiPage({context: context}, 
    ({headers, payload}) => {

      console.log("🦊 Wiki Page Event", payload.project.name)

      robot.send({
        message: `:fox_face: Wiki Page Event: ${ payload.project.web_url}`, 
        context: context
      })

    }
  )  


}
