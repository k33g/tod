console.log("> 🤖 brain | GitLab")

const GitLabClient = require('../../scm/gitlab/GitLabClient')

let gitLabTodCli = new GitLabClient({
  baseUri: process.env.GITLAB_INSTANCE_URL, 
  token: process.env.TOD_PERSONAL_ACCESS_TOKEN
})

module.exports =  ({robot, context}) =>  {

  /**
   * sample: search GitLab user information
   * 
   * eg: "@tod gitlab user info k33g" 
   * 
   * ⚠️ remark: don't put the @ before the handle, Slack translate it with the user slack id
   */
  robot.hearExactOrderWords(
    {sentence: "gitlab user info", context: context}, 
    ({message, channel, user, tokens}) => {

      console.log("😀 message", message)
      console.log("😀 tokens", tokens) //take the last token to get the handel

      let handle = tokens.slice(-1)[0]

      console.log("🦊 handle", handle)

      gitLabTodCli.fetchUserByHandle(handle)
        .then(data => {

          robot.send({
            message: robot.formatText(`
              :wave: Hey ${user.name} so you want some information about ${handle} ...
              Here are the informations:
              > id: ${data.username}
              > name: ${data.name}
              > state: ${data.state}
              > avatar_url: ${data.avatar_url}
              > web_url: ${data.web_url}
            `), 
            context: context
          })
        })
        .catch(err => {
          robot.send({
            message: robot.formatText(`
              :wave: Hey ${user.name} so you want some information about ${handle} ...
              But sorry :crying_cat_face: I can't find any information
            `), 
            context: context
          })
        }) // end of gitLabTodCli.fetchUserByHandle
    }
  )


  /**
   * sample: create a GitLab issue from the chat
   * 
   * eg:
   *   - "@tod gitlab create issue {k33g/hello-world} {this is the title}"
   *   - "@tod gitlab create issue {k33g/hello-world} {hello world 👋 🌍} {cc @k33g}"
   * 
   * 🐞 bug: if I use the word "issue" more than once eg for the title, the trigger is not triggered ...
   */
  robot.hearExactOrderWords(
    {sentence: "gitlab create issue", context: context}, 
    ({message, channel, user, tokens}) => {
      
      let extract = robot.extractTextBetweenCurlyBraces(message)
      // @tod gitlab create issue [k33g/hello-world] [this is the title] [hello world]
      //let extract = robot.extractTextBetweenBrackets(message)

      let nameSpace = extract[0].split("/")[0]
      let projectName = extract[0].split("/")[1]
      let title = extract[1]
      let description = extract[2] ? `> WIP :construction:\n ${extract[2]}` : `> WIP :construction:`

      // It's the todTheBot user (I use the web acces token of @todthebot - GitLab User)
      gitLabTodCli.createIssue({projectName, nameSpace, title, description:description})
        .then(payload => {
          
          robot.send({
            message: robot.formatText(`
              > 👍 \`${JSON.stringify(extract)}\` 
              > issue id/iid: \`${payload.id}/${payload.iid}\` created
            `), 
            context: context
          })
        })
        .catch(err => {
          robot.send({message: err, context: context})
        })
    
    }
  )


  /**
   * sample: get the content of file on gitlab
   * 
   * eg:
   *   - "@tod gitlab fetch file {k33g/hello-world} {documents/hello.md}"
   *   - "@tod gitlab fetch file {k33g/hello-world} {README.md}"
   *   - "@tod gitlab fetch file {k33g/hello-world} {tests.js}"
   */

  robot.hearExactOrderWords(
    {sentence: "gitlab fetch file", context: context}, 
    ({message, channel, user, tokens}) => {
      let extract = robot.extractTextBetweenCurlyBraces(message)

      let nameSpace = extract[0].split("/")[0]
      let projectName = extract[0].split("/")[1]
      let filePath = extract[1]

      gitLabTodCli.fetchRawFile({projectName, nameSpace, filePath, ref: "master" })
      .then(payload => {
        robot.send({message: "```"+payload+"```", context: context})
      })
      .catch(err => {
        robot.send({message: err, context: context})
      })

    }
  )

}