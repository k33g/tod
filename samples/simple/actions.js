console.log("> 🤖 brain | simple actions")

module.exports =  ({robot, context}) =>  {

  robot.hearRandomOrderWords(
    {sentence: "give price apple", context: context}, 
    ({message, channel, user, tokens, ngrams}) => {

      console.log(context.hello())
      console.log(context.hi())

      let answer = `💵 ${user.name}, this is the pricing`
      robot.send({
        message: answer, 
        context: context
      })
    }
  )

  robot.hearRandomOrderWords(
    {sentence: "how are you", context: context}, 
    ({message, channel, user, tokens, ngrams}) => {
      robot.send({
        message: `👋 Hey ${user.name} from ${channel.name}`, 
        context: context
      })
    }
  )

}