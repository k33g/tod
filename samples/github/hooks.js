console.log("> 🤖 brain | GitHub hooks")

module.exports =  ({robot, context}) =>  {

  robot.onGitHubEvent({context: context}, 
    ({headers, payload}) => {

      console.log("🐙", headers["host"], "event:", headers["x-github-event"])

      robot.send({
        message: `:octopus: host: ${headers["host"]} \n event: ${headers["x-github-event"]}`, 
        context: context
      })
    }
  )

  robot.onGitHubIssue({context: context}, 
    ({headers, payload}) => {
      //console.log(payload)
      robot.send({
        message: `:octopus: Issue by ${payload.sender.login}`, 
        context: context
      })
    }
  )

}
