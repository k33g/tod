console.log("> 🤖 brain | simple actions")

module.exports =  ({robot, context}) =>  {


  /**
   * === Jaro-Winkler distance ===
   * 
   * similarityTrigger (default value is 0.8) 
   * if the Jaro-Winkler distance is > to the similarityTrigger then the robot answers
   */ 
  robot.hearJWDistance(
    {sentence: "how are you", context: context, similarityTrigger: 0.7}, 
    ({message, channel, user, jaroWinklerDistance}) => {

      robot.send({
        message: robot.formatText(`
          > distance [Jaro-Winkler]: ${jaroWinklerDistance}
          > :wave: ${user.name} I'm fine :relaxed:
        `), 
        context: context
      })
    }
  ) 

  /**
   * === Levenshtein distance ===
   * 
   * similarityTrigger (default value is 5) 
   * if Levenshtein distance is <= to similarityTrigger then the robot answers
   * 
   * eg: sentence: "what up buddy"
   *   - what’s up buddy: 3
   *   - what’s up buddy?: 4
   *   - what up buddy: 1 (best)
   *   - what’s up bud?: 5
   */
  robot.hearLDistance(
    {sentence: "what up buddy", context: context, similarityTrigger: 5}, 
    ({message, channel, user, levenshteinDistance}) => {

      robot.send({
        message: robot.formatText(`
          > distance [Levenshtein]: ${levenshteinDistance}
          > :wave: ${user.name} the weather is sunny :sunny:
        `), 
        context: context
      })
    }
  ) 


  /**
   * === Dice's coefficient ===
   * 
   * similarityTrigger (default value is 0.75) 
   * if Dice's coefficient is < to similarityTrigger then the robot answers
   * 
   * eg: sentence: "what your real name"
   *   - what your real name -> 1
   *   - what’s your real name ? -> 0.9230769230769231
   *   - what’s your real name? -> 0.8888888888888888
   *   - what’s your name? -> 0.75
   */
  robot.hearDiceCoefficient(
    {sentence: "what your real name", context: context, similarityTrigger: 0.75}, 
    ({message, channel, user, diceCoefficient}) => {

      robot.send({
        message: robot.formatText(`
          > Coefficient [Dice]: ${diceCoefficient} 
          > :wave: ${user.name}, I'm Tod The Bot akka :fox_face: :robot_face:
        `), 
        context: context
      })
    }
  ) 

}