
class Robot {
  // TODO: how to manage the memory of the bot?
  // see the key-value db I used
  // or mongoDb ?
  // or the SCM
  constructor({name, trigger}, ...features) {
    this.name = name
    this.trigger = trigger
    return Object.assign(this, ...features)
  }
  message(data) {
    console.log(data)
  }

  hearAll({context: context}, callback) {
    // message without trigger
    // let message = context.message().replace(this.trigger, "")

    /* 
      If the text comes from an issue
      we dont remove the trigger eg `@tod`
      but we have to check if an other trigger is in the body of the issue
      ```javascript
        robot.hearExactOrderWords(
          {words:"@todthebot btw are you ok".split(" "), context: gitLabContext}, ...

        robot.hearDiceCoefficient(
          {words:["what", "your", "real", "name"], context: gitLabContext, similarityTrigger: 0.8},  ...
      ```
    */
    let message = context.isChatContext 
      ? context.message().replace(this.trigger, "")
      : context.message()

    let user = context.user()
    let channel = context.channel()
    callback({message, channel, user})
  }

  send({message, context}) {
    return context.send(message)
      .catch(err => console.log(`😡 Robot.answer`, err))
  }
  answer({message, context}) {
    console.log("🎃", message)
    return context.answer(message)
      .catch(err => console.log(`😡 Robot.answer`, err))
  }

}

module.exports = Robot
