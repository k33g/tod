console.log("> 🤖 adding features to the robot | text")

function formatText(text) { return text.split("\n").map(item => item.trim()).join("\n") }

function extractTextBetweenCurlyBraces(text) {
  let found = [], rxp = /{([^}]+)}/g, curMatch
  while(curMatch = rxp.exec(text)) {
    found.push(curMatch[1])
  }
  return found
}

function extractTextBetweenBrackets(text) {
  let found = [], rxp = /\[(.*?)\]/g, curMatch
  while(curMatch = rxp.exec(text)) {
    found.push(curMatch[1])
  }
  return found
}


module.exports = {
  formatText: formatText,
  format:formatText,
  extractTextBetweenCurlyBraces: extractTextBetweenCurlyBraces,
  extractTextBetweenBrackets: extractTextBetweenBrackets
};