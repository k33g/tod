console.log("> 🤖 adding features to the robot | hooks GitLab events")
//const natural = require('natural')

const TOKEN_OK = 1
const TOKEN_KO = -1
const NO_TOKEN = 0

function checkToken({headers}) {

  if(headers["x-gitlab-token"]) {
    if(headers["x-gitlab-token"]==process.env.GITLAB_HOOK_TOKEN) {
      return TOKEN_OK
    } else {
      //console.log("😡 GitLabEvent: probaly bad token")
      return TOKEN_KO
    }
  } else { // no token required
    return NO_TOKEN
  }
}

function isGitLabEvent({headers}) {

  if(headers["x-gitlab-event"]) {
    return true
  } else {
    return false
  }
}

function execute({headers, payload, callback}) {
  let data = {
    headers: headers, 
    payload: payload
  }
  switch(checkToken({headers})) {
    case TOKEN_OK: 
      callback(data)
      break
    case NO_TOKEN:
      callback(data)
      break
    default:
    console.log("😡 GitLabEvent: probaly bad token")
  }
}


function onGitLabEvent({context: context}, callback) {
  // process.env.GITLAB_HOOK_TOKEN
  let headers = context.headers()
  let payload = context.body()

  if(isGitLabEvent({headers})) {
    execute({headers, payload, callback})
  }
}

// payload.object_kind=="issue"
function onGitLabIssue({context: context}, callback) {
  let headers = context.headers()
  let payload = context.body()

  if(isGitLabEvent({headers}) && headers["x-gitlab-event"] == "Issue Hook") {
    execute({headers, payload, callback})
  }
}

function onGitLabConfidentialIssue({context: context}, callback) {
  let headers = context.headers()
  let payload = context.body()

  if(isGitLabEvent({headers}) && headers["x-gitlab-event"] == "Confidential Issue Hook") {
    execute({headers, payload, callback})
  }
}

// Comment on a issue
function onGitLabNote({context: context}, callback) {
  let headers = context.headers()
  let payload = context.body()

  if(isGitLabEvent({headers}) && headers["x-gitlab-event"] == "Note Hook") {
    execute({headers, payload, callback})
  }
}

function onGitLabPush({context: context}, callback) {
  let headers = context.headers()
  let payload = context.body()

  if(isGitLabEvent({headers}) && headers["x-gitlab-event"] == "Push Hook") {
    execute({headers, payload, callback})
  }
}

function onGitLabTagPush({context: context}, callback) {
  let headers = context.headers()
  let payload = context.body()

  if(isGitLabEvent({headers}) && headers["x-gitlab-event"] == "Tag Push Hook") {
    execute({headers, payload, callback})
  }
}

function onGitLabMergeRequest({context: context}, callback) {
  let headers = context.headers()
  let payload = context.body()

  if(isGitLabEvent({headers}) && headers["x-gitlab-event"] == "Merge Request Hook") {
    execute({headers, payload, callback})
  }
}

function onGitLabPipeLine({context: context}, callback) {
  let headers = context.headers()
  let payload = context.body()

  if(isGitLabEvent({headers}) && headers["x-gitlab-event"] == "Pipeline Hook") {
    execute({headers, payload, callback})
  }
}

function onGitLabJob({context: context}, callback) {
  let headers = context.headers()
  let payload = context.body()

  if(isGitLabEvent({headers}) && headers["x-gitlab-event"] == "Job Hook") {
    execute({headers, payload, callback})
  }
}

function onGitLabWikiPage({context: context}, callback) {
  let headers = context.headers()
  let payload = context.body()

  if(isGitLabEvent({headers}) && headers["x-gitlab-event"] == "Wiki Page Hook") {
    execute({headers, payload, callback})
  }
}

module.exports = {
  onGitLabEvent: onGitLabEvent,
  onGitLabIssue: onGitLabIssue,
  onGitLabConfidentialIssue:onGitLabConfidentialIssue,
  onGitLabPush: onGitLabPush,
  onGitLabTagPush: onGitLabTagPush,
  onGitLabNote: onGitLabNote,
  onGitLabMergeRequest: onGitLabMergeRequest,
  onGitLabPipeLine: onGitLabPipeLine,
  onGitLabJob: onGitLabJob,
  onGitLabWikiPage: onGitLabWikiPage
};
