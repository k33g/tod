console.log("> 🤖 adding features to the robot | hooks events")
//const natural = require('natural')

function onHookEvent({context: context}, callback) {


  callback({
    headers: context.headers(), 
    payload: context.body()
  })
  /*
    channel: context.channel(), 
    user: context.user()
  */

}

module.exports = {
  onHookEvent: onHookEvent
};