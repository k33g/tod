console.log("> 🤖 adding features to the robot | hooks GitHub events")
//const natural = require('natural')
const crypto = require('crypto')

const TOKEN_OK = 1
const TOKEN_KO = -1
const NO_TOKEN = 0

function checkToken({headers, payload}) {

  if(headers["x-hub-signature"]) {
    let hmac = crypto.createHmac("sha1", process.env.GITHUB_HOOK_TOKEN)
    let calculatedSignature = "sha1=" + hmac.update(JSON.stringify(payload)).digest("hex")

    console.log("🖖 calculatedSignature:", calculatedSignature)

    if(headers["x-hub-signature"]==calculatedSignature) {
      return TOKEN_OK
    } else {
      //console.log("😡 GitHubEvent: probaly bad token")
      return TOKEN_KO
    }
  } else { // no token required
    return NO_TOKEN
  }
}

function isGitHubEvent({headers}) {

  if(headers["x-github-event"]) {
    return true
  } else {
    return false
  }
}

function execute({headers, payload, callback}) {
  let data = {
    headers: headers, 
    payload: payload
  }
  switch(checkToken({headers, payload})) {
    case TOKEN_OK: 
      callback(data)
      break
    case NO_TOKEN:
      callback(data)
      break
    default:
    console.log("😡 GitHubEvent: probaly bad token")
  }
}

function onGitHubEvent({context: context}, callback) {

  let payload = context.body()
  let headers = context.headers()

  if(isGitHubEvent({headers})) {
    execute({headers, payload, callback})
  }

}

function onGitHubIssue({context: context}, callback) {
  let payload = context.body()
  let headers = context.headers()

  if(isGitHubEvent({headers}) && headers["x-github-event"] == "issues") {
    execute({headers, payload, callback})
  }
}

module.exports = {
  onGitHubEvent: onGitHubEvent,
  onGitHubIssue: onGitHubIssue
};