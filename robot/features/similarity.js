console.log("> 🤖 adding features to the robot | similarity[Jaro-Winkler|Levenshtein|Dice's Coefficient]")
const natural = require('natural')

// Jaro-Winkler
// best similarity -> 1
// worst similarity -> 0
function hearJWDistance({sentence: sentence, context: context, similarityTrigger=0.8}, callback) {

  let tokenizer = new natural.WordTokenizer()
  let words = tokenizer.tokenize(sentence)

  let message = context.message()
  let user = context.user()
  let channel = context.channel()

  // remove the trigger of the robot from the message
  // process.env.BOT_TRIGGER

  /* 
    If the text comes from an issue
    we dont remove the trigger eg `@tod`
    but we have to check if an other trigger is in the body of the issue
    ```javascript
      robot.hearExactOrderWords(
        {words:"@todthebot btw are you ok".split(" "), context: gitLabContext}, ...

      robot.hearDiceCoefficient(
        {words:["what", "your", "real", "name"], context: gitLabContext, similarityTrigger: 0.8},  ...
    ```
  */
 if(message) {
    let messageWithoutTrigger = context.isChatContext 
      ? message.replace(this.trigger, "")
      : message

    let jaroWinklerDistance = natural.JaroWinklerDistance(words.join(" "), messageWithoutTrigger)

    console.log("--------------------------------")
    console.log("🦊 words:", words)
    console.log("🐼 message:", messageWithoutTrigger)
    console.log("⚙️ JaroWinklerDistance:", jaroWinklerDistance)
    console.log("--------------------------------")

    if(jaroWinklerDistance > similarityTrigger) {
      callback({message, channel, user, jaroWinklerDistance})
    }
  }
}

// Levenshtein
// function hearLDistance
// best similarity -> 0
// worst similarity -> N
function hearLDistance({sentence: sentence, context: context, similarityTrigger=5}, callback) {
  
  let tokenizer = new natural.WordTokenizer()
  let words = tokenizer.tokenize(sentence)

  let message = context.message()
  let user = context.user()
  let channel = context.channel()

  /* 
    If the text comes from an issue
    we dont remove the trigger eg `@tod`
    but we have to check if an other trigger is in the body of the issue
    ```javascript
      robot.hearExactOrderWords(
        {words:"@todthebot btw are you ok".split(" "), context: gitLabContext}, ...

      robot.hearDiceCoefficient(
        {words:["what", "your", "real", "name"], context: gitLabContext, similarityTrigger: 0.8},  ...
    ```
  */
 if(message) {

    let messageWithoutTrigger = context.isChatContext 
      ? message.replace(this.trigger, "")
      : message
  
    let levenshteinDistance = natural.LevenshteinDistance(words.join(" "), messageWithoutTrigger)

    console.log("--------------------------------")
    console.log("🦊 words:", words)
    console.log("🐼 message:", messageWithoutTrigger)
    console.log("⚙️ LevenshteinDistance:", levenshteinDistance)
    console.log("--------------------------------")
    
    if(levenshteinDistance <= similarityTrigger) {
      callback({message, channel, user, levenshteinDistance})
    }
  }
}

// DiceCoefficient
function hearDiceCoefficient({sentence: sentence, context: context, similarityTrigger=0.75}, callback) {
  
  let tokenizer = new natural.WordTokenizer()
  let words = tokenizer.tokenize(sentence)

  let message = context.message()
  let user = context.user()
  let channel = context.channel()

  /* 
    If the text comes from an issue
    we dont remove the trigger eg `@tod`
    but we have to check if an other trigger is in the body of the issue
    ```javascript
      robot.hearExactOrderWords(
        {words:"@todthebot btw are you ok".split(" "), context: gitLabContext}, ...

      robot.hearDiceCoefficient(
        {words:["what", "your", "real", "name"], context: gitLabContext, similarityTrigger: 0.8},  ...
    ```
  */
 if(message) {
    let messageWithoutTrigger = context.isChatContext 
      ? message.replace(this.trigger, "")
      : message

    let diceCoefficient = natural.DiceCoefficient(words.join(" "), messageWithoutTrigger)

    console.log("--------------------------------")
    console.log("🦊 words:", words)
    console.log("🐼 message:", messageWithoutTrigger)
    console.log("⚙️ DiceCoefficient:", diceCoefficient)
    console.log("--------------------------------")

    if(diceCoefficient > similarityTrigger) {
      callback({message, channel, user, diceCoefficient})
    }
  }

}

module.exports = {
  hearJWDistance: hearJWDistance,
  hearLDistance: hearLDistance,
  hearDiceCoefficient: hearDiceCoefficient
};