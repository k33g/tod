console.log("> 🤖 adding features to the robot | words")
const natural = require('natural')

//let NGrams = natural.NGrams
//let stemmer = natural.PorterStemmer

// TODO: 
// - Add other hear methods (with different tokenizer)
// - See how to train the bot

/* give the all the words, what ever the order, but the word must to be consecutive*/
function hearRandomOrderWords({sentence: sentence, context: context}, callback) {
    
  let tokenizer = new natural.WordTokenizer()
  let words = tokenizer.tokenize(sentence)
  /* 
    If the text comes from an issue
    we dont remove the trigger eg `@tod`
    but we have to check if an other trigger is in the body of the issue
    ```javascript
      robot.hearExactOrderWords(
        {words:"@todthebot btw are you ok".split(" "), context: gitLabContext}, ...

      robot.hearDiceCoefficient(
        {words:["what", "your", "real", "name"], context: gitLabContext, similarityTrigger: 0.8},  ...
    ```
  */
  
  
  let message = context.isChatContext 
    ? context.message().replace(this.trigger, "")
    : context.message()

  if(message) {
    let user = context.user()
    let channel = context.channel()

    let tokens = tokenizer.tokenize(message)
    //let tokens = stemmer.tokenizeAndStem(message) <-- re try with this
    
    // check that all waited words are in the message
    let areAllTheWordsInTheMessage = words.every(word => tokens.find(token=>token==word))

    console.log("--------[🎲 Random Order]-----------")
    console.log("✉️ message:", message)
    console.log("🦊 words:", words)
    console.log("🐼 tokens:", tokens)
    console.log(areAllTheWordsInTheMessage ? "😀" : "😡", " result:", areAllTheWordsInTheMessage)
    console.log("--------------------------------")

    if(areAllTheWordsInTheMessage) {
      callback({message, channel, user, tokens})
    } else {
      //console.log(`🦊🤖 I don't understand`)
    }
  }
}

/* give the all the words, what ever the order, but the word must to be consecutive*/
function hearExactOrderWords({sentence: sentence, context: context}, callback) {
  // TODO: try with other tokenizers
  let tokenizer = new natural.WordTokenizer()
  let words = tokenizer.tokenize(sentence)

  /* 
    If the text comes from an issue
    we dont remove the trigger eg `@tod`
    but we have to check if an other trigger is in the body of the issue
    ```javascript
      robot.hearExactOrderWords(
        {words:"@todthebot btw are you ok".split(" "), context: gitLabContext}, ...

      robot.hearDiceCoefficient(
        {words:["what", "your", "real", "name"], context: gitLabContext, similarityTrigger: 0.8},  ...
    ```
  */
  
  
  let message = context.isChatContext 
    ? context.message().replace(this.trigger, "")
    : context.message()

  if(message) {
    let user = context.user()
    let channel = context.channel()

    let tokens = tokenizer.tokenize(message)
    
    // extract the items in the real order from the message
    let extractedItems = tokens.filter(item => words.includes(item))
    let allWordsCorrectOrder = words.join("-") == extractedItems.join("-")
    
    console.log("--------[🗂 Exact Order]-----------")
    console.log("✉️ message:", message)
    console.log("🦊 words:", words)
    console.log("🐼 tokens:", tokens)
    console.log("🐯 extracted:", extractedItems)
    console.log(allWordsCorrectOrder ? "😀" : "😡", " result:", allWordsCorrectOrder)
    console.log("--------------------------------")

    if(allWordsCorrectOrder) {
      callback({message, channel, user, tokens})
    } else {
      //console.log(`🦊🤖 I don't understand`)
    }
  }

}

function hearWordsInTheMessage({sentence: sentence, context: context}, callback) {
  
  let tokenizer = new natural.WordTokenizer()
  let words = tokenizer.tokenize(sentence)

  let message = context.isChatContext 
    ? context.message().replace(this.trigger, "")
    : context.message()
  
  if(message) {
    let user = context.user()
    let channel = context.channel()

    let tokens = tokenizer.tokenize(message)

    //let message_set = new Set(tokens)
    //let sentence_set = new Set(words)

    var res = false
    words.forEach(item => {
      if(tokens.includes(item)) {
        res = true
      } else {
        res = false
      }
    })

    if(res) {
      //console.log("===HAPPY 😀===")
      callback({message, channel, user, tokens})
    } else {
      //console.log("===NOT HAPPY 😡===")
    }
  }
}

module.exports = {
  hearRandomOrderWords: hearRandomOrderWords,
  hearExactOrderWords: hearExactOrderWords,
  hearWordsInTheMessage: hearWordsInTheMessage
};